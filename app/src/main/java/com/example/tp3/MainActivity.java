package com.example.tp3;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    SportDbHelper sportDbHelper = null;
    SimpleCursorAdapter cursorAdapter;
    int ADD_TEAM_REQUEST = 1;
    int UPDATE_TEAM_REQUEST = 2;
    RecyclerView teamRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipRefresh);

        sportDbHelper = new SportDbHelper(this);
        if (sportDbHelper.fetchAllTeams().getCount() < 1) {
            sportDbHelper.populate();
        }

        teamRecyclerView = (RecyclerView) findViewById(R.id.teamRecyclerView);
        teamRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        teamRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        teamRecyclerView.setAdapter(new IconicAdapter());


        // Drag and drop / Swipe
        ItemTouchHelper.Callback itemToucherHelperCallback = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int dragFlags = 0;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                Team team = sportDbHelper.getAllTeams().get(position);
                sportDbHelper.deleteTeam((int) team.getId());
                refresh();
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemToucherHelperCallback);
        itemTouchHelper.attachToRecyclerView(teamRecyclerView);



        swipeRefreshLayout.setOnRefreshListener(() -> {

            ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                WebUpdateTeam webUpdateTeam = new WebUpdateTeam();
                webUpdateTeam.setup(sportDbHelper.getAllTeams(),this);
                webUpdateTeam.execute();
            }
            else {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Connection impossible")
                        .setMessage("Veuillez verifier votre connexion internet, puis réessayer.")
                        .setPositiveButton("D'accord",null)
                        .show();
            }


        });
        /*cursorAdapter =
                new SimpleCursorAdapter(this,
                        android.R.layout.simple_list_item_2, sportDbHelper.fetchAllTeams(), new String[] {
                        sportDbHelper.COLUMN_TEAM_NAME,
                        sportDbHelper.COLUMN_LEAGUE_NAME },
                        new int[] { android.R.id.text1, android.R.id.text2 }, 0);



        ListView teamListView = (ListView) findViewById(R.id.list_team);

        teamListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Team teamSelected = sportDbHelper.getTeam(id);

                //Toast.makeText(MainActivity.this, "LA TEAM !!! -> "+ teamSelected.getName(), Toast.LENGTH_SHORT).show();

                Intent teamView = new Intent(MainActivity.this, TeamActivity.class);
                teamView.putExtra(Team.TAG,teamSelected);
                startActivityForResult(teamView,UPDATE_TEAM_REQUEST);
            }
        });

        teamListView.setAdapter(cursorAdapter);*/


        // New team
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newTeamView = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivityForResult(newTeamView,ADD_TEAM_REQUEST);
            }
        });
    }


    // Adapter (controler de ligne)
    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        List<Team> listTeam = sportDbHelper.getAllTeams();

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.team_item_row, parent, false)));
        }
        @Override
        public void onBindViewHolder(RowHolder holder, int position) {

            Team teamSelected = listTeam.get(position);

            holder.bindModel(teamSelected);
        }
        @Override
        public int getItemCount() {
            return(listTeam.size());
        }
    }

    // Gestionnaire d'affichage d'une ligne

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView logoImageView;

        TextView teamNameTextView;
        TextView teamStadiumTextView ;
        TextView leagueNameTextView;

        List<Team> listTeam = sportDbHelper.getAllTeams();

        RowHolder(View row) {
            super(row);

            logoImageView = (ImageView) row.findViewById(R.id.logoImageView);

            teamNameTextView = (TextView) row.findViewById(R.id.teamNameTextView);
            teamStadiumTextView =(TextView) row.findViewById(R.id.teamStadiumTextView);
            leagueNameTextView = (TextView) row.findViewById(R.id.leagueNameTextView);
            row.setOnClickListener(this);
        }
        @Override
        public void onClick(View v){
            int itemPosition = teamRecyclerView.getChildLayoutPosition(v);

            Team teamSelected = listTeam.get(itemPosition);
            //Toast.makeText(MainActivity.this, "LA TEAM !!! -> "+ teamSelected.getName(), Toast.LENGTH_SHORT).show();

            Intent teamView = new Intent(MainActivity.this, TeamActivity.class);
            teamView.putExtra(Team.TAG,teamSelected);
            startActivityForResult(teamView,UPDATE_TEAM_REQUEST);
        }
        void bindModel(Team team) {

            teamNameTextView.setText(team.getName());

            if (team.getStadium() != null) {
                teamStadiumTextView.setText("( "+team.getStadium()+" )");
            } else {
                teamStadiumTextView.setText("( Stade introuvable )");
            }

            leagueNameTextView.setText(team.getLeague());

            if (team.getTeamBadge() != null) {
                SetImageBadgeByUrlTask setImageBadgeByUrlTask = new SetImageBadgeByUrlTask(team, logoImageView);
                setImageBadgeByUrlTask.execute();
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_TEAM_REQUEST) {
            if (resultCode == RESULT_OK) {
                Team team = (Team) data.getParcelableExtra(Team.TAG);
                if (team != null) {
                    sportDbHelper.addTeam(team);
                    refresh();
                    //cursorAdapter.swapCursor(sportDbHelper.fetchAllTeams());
                }
            }
        }
        // Sauvegarde dans Worker.java
        if (requestCode == UPDATE_TEAM_REQUEST) {
            if (resultCode == RESULT_OK) {
                Team team = (Team) data.getParcelableExtra(Team.TAG);
                if (team != null) {
                    sportDbHelper.updateTeam(team);
                    refresh();
                    //cursorAdapter.swapCursor(sportDbHelper.fetchAllTeams());
                }
            }
        }
    }

    public void refresh() {
        finish();
        //overridePendingTransition(1, 1);
        startActivity(getIntent());
        //overridePendingTransition(0, 0);
    }
}
