package com.example.tp3;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.tp3.JSONResponseHandlerLastEvent;
import com.example.tp3.JSONResponseHandlerTeam;
import com.example.tp3.JSONResponseHandlerTeamRank;
import com.example.tp3.Team;
import com.example.tp3.WebServiceUrl;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Worker extends AsyncTask<URL,Integer, Team> {
    private boolean isLast;
    private Team team;
    private Activity activity;

    public Worker(Team team, boolean isLast, Activity activity) {
        this.team = team;
        this.isLast = isLast;
        this.activity = activity;
    }


    @Override
    protected Team doInBackground(URL... urls) {

        HttpURLConnection urlConnectionTeam;
        HttpURLConnection urlConnectionTeamRank;
        HttpURLConnection urlConnectionLastEvent;
        InputStream inputStreamTeam;
        InputStream inputStreamTeamRank;
        InputStream inputStreamLastEvent;
        JSONResponseHandlerTeam jsonResponseHandlerTeam = new JSONResponseHandlerTeam(team);
        JSONResponseHandlerTeamRank jsonResponseHandlerTeamRank = new JSONResponseHandlerTeamRank(team);
        JSONResponseHandlerLastEvent jsonResponseHandlerLastEvent = new JSONResponseHandlerLastEvent(team);

        try {
            urlConnectionTeam = (HttpURLConnection) urls[0].openConnection();
            try {
                inputStreamTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
                try {
                    jsonResponseHandlerTeam.readJsonStream(inputStreamTeam);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } finally {
                urlConnectionTeam.disconnect();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (team.getId() > 0) {
            try {
                URL apiURLLastEvent = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                urlConnectionLastEvent = (HttpURLConnection) apiURLLastEvent.openConnection();
                try {
                    inputStreamLastEvent = new BufferedInputStream(urlConnectionLastEvent.getInputStream());
                    jsonResponseHandlerLastEvent.readJsonStream(inputStreamLastEvent);

                } finally {
                    urlConnectionLastEvent.disconnect();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (team.getIdLeague() > 0) {
                try {
                    URL apiURLTeamRank = WebServiceUrl.buildGetRanking(team.getIdLeague());
                    urlConnectionTeamRank = (HttpURLConnection) apiURLTeamRank.openConnection();
                    try {
                        inputStreamTeamRank = new BufferedInputStream(urlConnectionTeamRank.getInputStream());
                        jsonResponseHandlerTeamRank.readJsonStream(inputStreamTeamRank);

                    } finally {
                        urlConnectionTeamRank.disconnect();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return team;
    }

    @Override
    protected void onPostExecute(Team team) {
        super.onPostExecute(team);
        SportDbHelper sportDbHelper = new SportDbHelper(activity);
        sportDbHelper.updateTeam(team);

        if (isLast) {
            Intent intent = activity.getIntent();
            activity.finish();
            activity.startActivity(intent);
        }
    }
}