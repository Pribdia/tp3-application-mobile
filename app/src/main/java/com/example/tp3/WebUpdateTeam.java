package com.example.tp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.view.View;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class WebUpdateTeam extends AsyncTask<URL,Integer,List<Team>> {

    List<Team> listTeam = null;
    Team team = null;
    Activity activity;

    public void setup(List<Team> listTeam, Activity activity) {
        this.listTeam = listTeam;
        this.activity = activity;
    }

    public void setup(Team team, Activity activity) {
        this.team = team;
        this.activity = activity;
    }

    @Override
    protected List<Team> doInBackground(URL... urls) {
            Worker worker;
            boolean isLast;
            for (int i = 0; i < listTeam.size(); i++) {
                try {

                    if (i == listTeam.size()-1) {
                        isLast = true;
                    } else {
                        isLast = false;
                    }
                    worker = new Worker(listTeam.get(i),isLast,activity);

                    if (worker.getStatus() == AsyncTask.Status.PENDING) {
                        URL apiUrlTeamSearch = WebServiceUrl.buildSearchTeam(listTeam.get(i).getName());
                        worker.execute(apiUrlTeamSearch);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        return listTeam;
    }

    @Override
    protected void onPostExecute(List<Team> teams) {
        super.onPreExecute();
    }
}
