package com.example.tp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;


public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;
    Intent intent;
    private Activity activity;

    SportDbHelper sportDbHelper = new SportDbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        intent = new Intent();
        activity = this;

        //SportDbHelper sportDbHelper = new SportDbHelper(this);
        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                    List<Team> listTeam = new ArrayList<Team>();
                    listTeam.add(team);
                    WebUpdateTeam webUpdateTeam = new WebUpdateTeam();
                    webUpdateTeam.setup(listTeam,TeamActivity.this);
                    webUpdateTeam.execute();
                }
                else {
                    new AlertDialog.Builder(TeamActivity.this)
                            .setTitle("Connection impossible")
                            .setMessage("Veuillez verifier votre connexion internet, puis réessayer.")
                            .setPositiveButton("D'accord",null)
                            .show();
                }



            }
        });

    }



    @Override
    public void onBackPressed() {
        Intent mainActivity = new Intent(TeamActivity.this, MainActivity.class);
        startActivity(mainActivity);
        /*intent.putExtra(Team.TAG, team);
        setResult(RESULT_OK, intent);*/
        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        if (team.getTeamBadge() != null) {
            SetImageBadgeByUrlTask setImageBadgeByUrlTask = new SetImageBadgeByUrlTask(team,imageBadge);
            setImageBadgeByUrlTask.execute();
        }


    }

}
