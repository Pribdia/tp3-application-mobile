package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id=135209
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerLastEvent {

    private static final String TAG = JSONResponseHandlerLastEvent.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerLastEvent(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readLastEvent(reader);
        } finally {
            reader.close();
        }
    }

    public void readLastEvent(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results") && reader.peek() == JsonToken.BEGIN_ARRAY) {
                readArrayLastEvent(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayLastEvent(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        Match match = new Match();
        //long id, String label, String homeTeam, String awayTeam, int homeScore, int awayScore
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    if (name.equals("idEvent")) {
                        match.setId(reader.nextLong());
                    } else if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")) {
                        if (reader.peek() != JsonToken.NULL) {
                            match.setHomeScore(reader.nextInt());
                        } else {
                            match.setHomeScore(-1);
                            reader.skipValue();
                        }
                    } else if (name.equals("intAwayScore")) {
                        if (reader.peek() != JsonToken.NULL) {
                            match.setAwayScore(reader.nextInt());
                        } else {
                            match.setAwayScore(-1);
                            reader.skipValue();
                        }
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        team.setLastEvent(match);
        reader.endArray();
    }

}
