package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeamRank {

    private static final String TAG = JSONResponseHandlerTeamRank.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerTeamRank(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeamRanks(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeamRanks(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (reader.peek() == JsonToken.BEGIN_ARRAY && name.equals("table")) {
                readArrayTeamRanks(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeamRanks(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 1; // only consider the first element of the array
        boolean isTeam = false;
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if(name.equals("name") && reader.nextString().equals(team.getName())){
                    while (reader.hasNext()) {
                        name=reader.nextName();
                        if ("total".equals(name)) {
                            team.setRanking(nb);
                            team.setTotalPoints(reader.nextInt());
                        } else {
                            reader.skipValue();
                        }
                    }
                } else {
                    while(reader.hasNext()){
                        reader.skipValue();
                    }
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
