package com.example.tp3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import java.io.InputStream;

public class SetImageBadgeByUrlTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;
    Team team;
    public SetImageBadgeByUrlTask(Team team, ImageView bmImage) {
        this.bmImage = bmImage;
        this.team = team;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = team.getTeamBadge();
        Bitmap bmp = null;
        try {
            if (urldisplay != null) {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bmp;
    }
    protected void onPostExecute(Bitmap result) {

        RotateAnimation rotate = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);

        rotate.setDuration(4000);
        bmImage.setAnimation(rotate);
        bmImage.setImageBitmap(result);

    }
}
